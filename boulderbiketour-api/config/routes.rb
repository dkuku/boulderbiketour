Rails.application.routes.draw do

  get('status' => 'status#index')

  namespace :api do
    namespace :v1 do
      resources :riders
      resources :slogans
    end
  end
  
  get '*path', to: "application#fallback_index_html", constraints: ->(request) do
    !request.xhr? && request.format.html?
  end
end
