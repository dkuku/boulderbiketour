import React, {Component} from 'react';
import Countdown from './Countdown';
import NewsletterForm from './NewsletterForm';

export default class MainTopPart extends Component {
render(){
    return (
        <div className="align-items-center d-flex photo-overlay py-5 cover" style={{backgroundImage: "url(./home.jpg)"}} >
            <div className="container">
                <div className="row">
                    <div className="text-light text-shadow col-lg-7 align-self-center text-lg-left text-center">
                        <h1 className="mb-0 mt-4 display-3">BoulderBikeTour</h1>
                        <h3 className="mb-0 mt-4">Rocky Mountains, Colorado</h3>
                        <p className="mb-5 lead text-shadow-small">First April 2020</p>
                        <NewsletterForm />
                    </div>
                    <div className="text-light col-lg-5 p-3">
                        <h5 className="text-shadow-small">starts in:</h5> 
                        <Countdown />
                    </div>
                </div>
            </div>
        </div>
   )}
}
