import React, { Component } from 'react';
import Sponsors from './components/Sponsors';
import Highlights from './components/Highlights';
import MainTopPart from './components/MainTopPart';
import {Container} from 'reactstrap';

export default class Home extends Component {
    render(){
        return(
        <div>
            <MainTopPart />
            <div className="p-4" >
                <h2 className="text-info">Highlights</h2>
               <Highlights className="mx-auto"/> 
            </div>
            <Container fluid className="bg-light text-dark p-4" >
                <h2 className="text-info">Sponsors</h2>
                <Sponsors />
            </Container>
        </div>
        )

    }
}
